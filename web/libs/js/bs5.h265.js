$(document).ready(function(){
    onLiveStreamInitiate(function(streamType,monitor,loadedPlayer,subStreamChannel){
        if(streamType === 'h265'){
            var livePlayerElement = loadedLiveGrids[monitor.mid]
            var details = monitor.details
            var groupKey = monitor.ke
            var monitorId = monitor.mid
            var loadedMonitor = loadedMonitors[monitorId]
            var loadedPlayer = loadedLiveGrids[monitor.mid]
            var websocketPath = checkCorrectPathEnding(location.pathname) + 'socket.io'
            var containerElement = $(`#monitor_live_${monitor.mid}`)

            var player = loadedPlayer.h265Player
            var video = containerElement.find('.stream-element')[0]
            if (player) {
                player.stop()
                revokeVideoPlayerUrl(monitorId)
            }
            loadedPlayer.h265Player = new libde265.RawPlayer(video)
            var player = loadedPlayer.h265Player
            player.set_status_callback(function(msg, fps) {
            })
            player.launch()
            if(loadedPlayer.h265Socket && loadedPlayer.h265Socket.connected){
                loadedPlayer.h265Socket.disconnect()
            }
            if(loadedPlayer.h265HttpStream && loadedPlayer.abort){
                loadedPlayer.h265HttpStream.abort()
            }
            if(monitor.details.stream_flv_type==='ws'){
              loadedPlayer.h265Socket = io(location.origin,{ path: websocketPath, query: websocketQuery, transports: ['websocket'], forceNew: false})
              var ws = loadedPlayer.h265Socket
              ws.on('diconnect',function(){
                  console.log('h265Socket Stream Disconnected')
              })
              ws.on('connect',function(){
                  ws.emit('h265',{
                      auth: $user.auth_token,
                      uid: $user.uid,
                      ke: groupKey,
                      id: monitorId,
                      channel: subStreamChannel
                  })
                  ws.on('data',function(imageData){
                      player._handle_onChunk(imageData)
                  })
              })
            }else{
              var url = getApiPrefix(`h265`) + '/' + monitorId + (subStreamChannel ? `/${subStreamChannel}` : '') + '/s.hevc';
              loadedPlayer.h265HttpStream = player.createHttpStream(url)
            }
        }
    })
    onLiveStreamClose(function(livePlayerElement){
        // if(streamType === 'h265'){
            if(livePlayerElement.h265Socket){livePlayerElement.h265Socket.disconnect()}
            if(livePlayerElement.h265Player){livePlayerElement.h265Player.stop()}
            if(livePlayerElement.h265HttpStream && livePlayerElement.h265HttpStream.abort){
                livePlayerElement.h265HttpStream.abort()
            }
        // }
    })
    onSignalCheckLiveStream(function(streamType,monitorItem){
        if(streamType === 'h265'){
            monitorItem.resize()
        }
    })
    onGetSnapshotByStream(function(streamType,targetElement,completeAction){
        if(streamType === 'h265'){
            var c = targetElement[0]
            var ctx = c.getContext('2d')
            completeAction(atob(c.toDataURL('image/jpeg').split(',')[1]),c.width,c.height)
        }
    })
    onBuildStreamUrl(function(streamType,monitorId){
        if(streamType === 'h265'){
            return getApiPrefix(`h265`) + '/' + monitorId + '/s.hevc'
        }
    })
    onBuildStreamElement(function(streamType){
        if(streamType === 'h265'){
            return '<canvas class="stream-element"></canvas>';
        }
    })
    console.log('Loaded H.265 Library')
})
