module.exports = (s,config,lang,app,io) => {
    function findFieldByName(pageTag,blockTag,fieldName){
        const fields = s.definitions[pageTag].blocks[blockTag].info
        const index = fields.findIndex(field => field.name === fieldName)
        return fields[index]
    }
    function findFieldInSubStreamOutputByName(fieldName){
        const fields = findFieldByName('Monitor Settings','Substream',lang['Output']).info;
        const fieldIndex = fields.findIndex(field => field.name === fieldName);
        return fields[fieldIndex]
    }
    findFieldByName('Monitor Settings','Stream','detail=stream_type').possible.push({
        "name": lang['HEVC (H.265)'],
        "value": "h265"
    });
    findFieldByName('Monitor Settings Additional Stream Channel','Stream',`channel-detail="stream_type"`).possible.push({
        "name": lang['HEVC (H.265)'],
        "value": "h265"
    });
    findFieldInSubStreamOutputByName(`detail-substream-output="stream_type"`).possible.push({
        "name": lang['HEVC (H.265)'],
        "value": "h265"
    });
    ([
        'detail=stream_flv_type',
        'detail=stream_vcodec',
        'detail=stream_acodec',
        'detail=hls_time',
        'detail=hls_list_size',
        'detail=preset_stream',
        'detail=stream_quality',
        'detail=stream_fps',
        'detail=stream_scale_x',
        'detail=stream_scale_y',
        'detail=stream_rotate',
        'detail=stream_vf',
    ]).forEach((fieldName) => {
        findFieldByName('Monitor Settings','Stream',fieldName)["form-group-class"] += ' h_st_h265';
    });
    ([
        `detail-substream-output="stream_vcodec"`,
        `detail-substream-output="stream_acodec"`,
        `detail-substream-output=stream_quality`,
        `detail-substream-output=stream_v_br`,
        `detail-substream-output=stream_a_br`,
        `detail-substream-output=stream_fps`,
        `detail-substream-output=stream_scale_x`,
        `detail-substream-output=stream_scale_y`,
        `detail-substream-output=stream_rotate`,
        `detail-substream-output=svf`,
    ]).forEach((fieldName) => {
        const fields = findFieldByName('Monitor Settings','Substream',lang['Output']).info;
        const fieldIndex = fields.findIndex(field => field.name === fieldName);
        fields[fieldIndex]["form-group-class"] += ' h_st_channel_SUBSTREAM_FIELDS_h265';
    });
    ([
        `channel-detail="stream_vcodec"`,
        `channel-detail="stream_acodec"`,
        `channel-detail=stream_quality`,
        `channel-detail=stream_v_br`,
        `channel-detail=stream_a_br`,
        `channel-detail=stream_fps`,
        `channel-detail=stream_scale_x`,
        `channel-detail=stream_scale_y`,
        `channel-detail=stream_rotate`,
        `channel-detail=svf`,
    ]).forEach((fieldName) => {
        findFieldByName('Monitor Settings Additional Stream Channel','Stream',fieldName)["form-group-class"] += ' h_st_channel_$[TEMP_ID]_h265';
    });
    s.onProcessReady(function(){
        setTimeout(() => {
            config.workerStreamOutHandlers.push('h265')
            config.outputsWithAudio.push('h265')
            config.outputsNotCapableOfPresets.push('h265')
        },2000)
    })
    s.onFfmpegBuildMainStream((streamType,streamFlags) => {
        if(streamType === 'h265'){
            streamFlags.push(`-movflags +frag_keyframe+empty_moov+default_base_moof -metadata title="Shinobi H.265 Stream" -reset_timestamps 1 -f hevc pipe:1`)
        }
    })
    s.onFfmpegBuildStreamChannel((streamType,streamFlags,number) => {
        if(streamType === 'h265'){
            streamFlags.push(`-movflags +frag_keyframe+empty_moov+default_base_moof -metadata title="Shinobi H.265 Stream" -reset_timestamps 1 -f hevc pipe:${number}`)
        }
    })
    s.onMonitorCreateStreamPipe((streamType,e,resetStreamCheck) => {
        if(streamType === 'h265'){
            return function(d){
                resetStreamCheck(e)
                s.group[e.ke].activeMonitors[e.id].emitter.emit('data',d)
            }
        }
    })

    // WEBSOCKET Stream Handler
    s.onWebSocketConnection(function(cn,validatedAndBindAuthenticationToSocketConnection,createStreamEmitter){
        var tx;
        //unique h265 socket stream
        cn.on('h265',function(d){
            if(!s.group[d.ke]||!s.group[d.ke].activeMonitors||!s.group[d.ke].activeMonitors[d.id]){
                cn.disconnect();return;
            }
            cn.ip=cn.request.connection.remoteAddress;
            var toUTC = function(){
                return new Date().toISOString();
            }
            var tx=function(z){cn.emit('data',z);}
            const onFail = (msg) => {
                tx({f:'stop_reconnect',msg:msg,token_used:d.auth,ke:d.ke});
                cn.disconnect();
            }
            const onSuccess = (r) => {
                r = r[0];
                const Emitter = createStreamEmitter(d,cn)
                validatedAndBindAuthenticationToSocketConnection(cn,d,true)
                var contentWriter
                cn.closeSocketVideoStream = function(){
                    Emitter.removeListener('data', contentWriter);
                }
                Emitter.on('data',contentWriter = function(base64){
                    tx(base64)
                })
             }
            //check if auth key is user's temporary session key
            if(s.group[d.ke]&&s.group[d.ke].users&&s.group[d.ke].users[d.auth]){
                onSuccess(s.group[d.ke].users[d.auth]);
            }else{
                streamConnectionAuthentication(d,cn.ip).then(onSuccess).catch(onFail)
            }
        })
    });


    // HTTP Stream Handler
    function noCache(res){
        res.setHeader('Cache-Control', 'private, no-cache, no-store, must-revalidate')
        res.setHeader('Expires', '-1')
        res.setHeader('Pragma', 'no-cache')
    }
    /**
    * API : Get H.265/h265 HEVC stream
    */
    app.get([config.webPaths.apiPrefix+':auth/h265/:ke/:id/s.hevc',config.webPaths.apiPrefix+':auth/h265/:ke/:id/:channel/s.hevc'], function(req,res) {
        s.auth(req.params,function(user){
            s.checkChildProxy(req.params,function(){
                noCache(res)
                var Emitter,chunkChannel
                if(!req.params.channel){
                    Emitter = s.group[req.params.ke].activeMonitors[req.params.id].emitter
                    chunkChannel = 'MAIN'
                }else{
                    Emitter = s.group[req.params.ke].activeMonitors[req.params.id].emitterChannel[parseInt(req.params.channel)+config.pipeAddition]
                    chunkChannel = parseInt(req.params.channel)+config.pipeAddition
                }
                //variable name of contentWriter
                var contentWriter
                //set headers
                res.setHeader('Content-Type', 'video/mp4');
                res.setHeader('Access-Control-Allow-Origin','*');
                var ip = s.getClientIp(req)
                s.camera('watch_on',{
                    id : req.params.id,
                    ke : req.params.ke
                },{
                    id : req.params.auth + ip + req.headers['user-agent']
                })
                //write new frames as they happen
                if(!Emitter){
                    res.end('No Stream')
                    return
                }
                Emitter.on('data',contentWriter=function(buffer){
                    res.write(buffer)
                })
                //remove contentWriter when client leaves
                res.on('close', function () {
                    Emitter.removeListener('data',contentWriter)
                    s.camera('watch_off',{
                        id : req.params.id,
                        ke : req.params.ke
                    },{
                        id : req.params.auth + ip + req.headers['user-agent']
                    })
                })
            },res,req)
        },res,req)
    })
}
